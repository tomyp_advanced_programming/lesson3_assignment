#include "sqlite3.h"
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>

std::unordered_map<std::string, std::vector<std::string>> results;
void clearTable()
{
	for (auto it = results.begin(); it != results.end(); ++it)
	{
		it->second.clear();
	}
	results.clear();
}

void printTable()
{
	auto iter = results.end();
	iter--;
	//	int size = results.begin()->second.size();
	int size = iter->second.size();
	for (int i = -2; i < size; i++)
	{
		for (auto it = results.begin(); it != results.end(); ++it)
		{
			if (i == -2)
			{
				//cout << it->first << " ";
				printf("|%*s|", 13, it->first.c_str());
			}
			else if (i == -1)
				std::cout << "_______________";
			else
				//	cout << it->second.at(i) << " ";
				printf("|%*s|", 13, it->second.at(i).c_str());
		}
		std::cout << std::endl;
	}
}

int callback(void* notUsed, int argc, char** argv, char** azCol)
{
	int i;

	for (i = 0; i < argc; i++)
	{
		auto it = results.find(azCol[i]);
		if (it != results.end())
		{
			it->second.push_back(argv[i]);
		}
		else
		{
			std::pair<std::string, std::vector<std::string>> p;
			p.first = azCol[i];
			p.second.push_back(argv[i]);
			results.insert(p);
		}
	}

	return 0;
}

int getLastId(void* notUsed, int argc, char** argv, char** azCol)
{
	std::cout << "the Last id is: " << argv[0] << std::endl;
	return 0;
}


int main()
{
	int rc;
	sqlite3* db;
	char *zErrMsg = 0;
	bool flag = true;

	// connection to the database
	rc = sqlite3_open("FirstPart.db", &db);

	if (rc)
	{
		std::cout << "Can't open database: " << sqlite3_errmsg(db) << std::endl;
		sqlite3_close(db);
		system("Pause");
		return(1);
	}

	system("CLS");

	if (flag) 
	{
		std::cout << "insert into people(name) values('Gal');" << std::endl;
		std::cout << "insert into people(name) values('Alon');" << std::endl;
		std::cout << "insert into people(name) values('Sharon');" << std::endl;

	//	clearTable();

		rc = sqlite3_exec(db, "insert into people(name) values('Gal');", NULL, 0, &zErrMsg);
		rc = sqlite3_exec(db, "insert into people(name) values('Alon');", NULL, 0, &zErrMsg);
		rc = sqlite3_exec(db, "insert into people(name) values('Sharon');", NULL, 0, &zErrMsg);

		if (rc != SQLITE_OK)
		{
			std::cout << "SQL error: " << zErrMsg << std::endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			return 1;
		}	
	}
	clearTable();

	std::cout << "update people set name='Tommy' where id=(select MAX(id) from people);" << std::endl;

	rc = sqlite3_exec(db, "update people set name='Tommy' where id=(select MAX(id) from people);", NULL, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		std::cout << "SQL error: " << zErrMsg << std::endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}

	system("Pause");
	system("CLS");

}