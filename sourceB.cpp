#include <iostream>
#include <vector>
#include <string>
#include <unordered_map>
#include "sqlite3.h"

std::unordered_map<std::string, std::vector<std::string>> results;
int acc_balance;
int car_av;
int car_price;
int pr(void *data, int argc, char **argv, char **azColName) {
	int i;
	fprintf(stderr, "%s: ", (const char*)data);

	for (i = 0; i<argc; i++) {
		printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
	}
	printf("\n");
	return 0;
}

int callback(void* notUsed, int argc, char** argv, char** azCol)
{
	for (int i = 0; i < argc; i++)
	{
		auto it = results.find(azCol[i]);
		if (it != results.end())
		{
			it->second.push_back(argv[i]);
		}
		else
		{
			std::pair<std::string, std::vector<std::string>> p;
			p.first = azCol[i];
			p.second.push_back(argv[i]);
			results.insert(p);
		}
	}

	return 0;
}

int Price_Available(void* notUsed, int argc, char** argv, char** azCol)
{
	car_av = atoi(argv[0]);//set car i
	return 0;

}

int Balance(void* notUsed, int argc, char** argv, char** azCol)
{
	 acc_balance = atoi(argv[0]); //set buyer balance
	 return 0;

}
int Price_car(void* notUsed, int argc, char** argv, char** azCol)
{
	car_price = atoi(argv[0]); //get car price 
	return 0;

}


bool carPurchase(int buyerid, int carid, sqlite3* db, char* zErrMsg)
{
	int rc;

	std::string * balance = new std::string("SELECT balance FROM accounts WHERE Buyer_id=" + std::to_string(buyerid) + ";");
	std::string * car_available = new std::string("SELECT available FROM cars WHERE id=" + std::to_string(carid) + ";");
	std::string * price_car = new std::string("SELECT price FROM cars WHERE id=" + std::to_string(carid) + ";");

	rc = sqlite3_exec(db, balance->c_str(), Balance, 0, &zErrMsg); 
	
	if (rc != SQLITE_OK)
	{
		std::cout << "SQL error: " << zErrMsg << std::endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}

	rc = sqlite3_exec(db, car_available->c_str(), Price_Available, 0, &zErrMsg);
	
	if (rc != SQLITE_OK)
	{
		std::cout << "SQL error: " << zErrMsg << std::endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	rc = sqlite3_exec(db, price_car->c_str(), Price_car, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		std::cout << "SQL error: " << zErrMsg << std::endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	std::cout << car_av;
	if ((car_av == 0) || (car_price > acc_balance))
	{
		return false;
	}
	else
	{
		int theNewBalance = acc_balance - car_price;
		
		std::string * availableOrNot = new std::string("UPDATE cars SET available=0 WHERE id=" + std::to_string(carid) + ";");
		std::string * newBalance = new std::string("UPDATE accounts SET balance=" + std::to_string(theNewBalance) + " WHERE id=" + std::to_string(buyerid) + ";");
		
		rc = sqlite3_exec(db, availableOrNot->c_str(), NULL, 0, &zErrMsg);
		if (rc != SQLITE_OK)
		{
			std::cout << "SQL error: " << zErrMsg << std::endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			return 1;
		}

		rc = sqlite3_exec(db, newBalance->c_str(), NULL, 0, &zErrMsg);
		if (rc != SQLITE_OK)
		{
			std::cout << "SQL error: " << zErrMsg << std::endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			return 1;
		}
		return true;
	}
}

bool balanceTransfer(int from, int to, int amount, sqlite3* db, char* zErrMsg)
{
	int rc = 0;

	std::string * update1 = new std::string("UPDATE accounts SET balance=balance-" + std::to_string(amount) + " WHERE id=" + std::to_string(from) + ";");
	std::string * update2 = new std::string("UPDATE accounts SET balance=balance+" + std::to_string(amount) + " WHERE id=" + std::to_string(to) + ";");

	if (amount)
	{
		rc = sqlite3_exec(db, "begin transaction;", NULL, 0, &zErrMsg);
		if (rc != SQLITE_OK)
		{
			std::cout << "SQL error: " << zErrMsg << std::endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			return 1;
		}

		rc = sqlite3_exec(db, update1->c_str(), NULL, 0, &zErrMsg);
		if (rc != SQLITE_OK)
		{
			std::cout << "SQL error: " << zErrMsg << std::endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			return 1;
		}

		rc = sqlite3_exec(db, update2->c_str(), NULL, 0, &zErrMsg);
		if (rc != SQLITE_OK)
		{
			std::cout << "SQL error: " << zErrMsg << std::endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			return 1;
		}

		rc = sqlite3_exec(db, "commit;", NULL, 0, &zErrMsg);
		if (rc != SQLITE_OK)
		{
			std::cout << "SQL error: " << zErrMsg << std::endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			return 1;
		}
		return true;
	}
	else
	{
		return false;
	}
}

void whoCanBuy(int carId, sqlite3* db, char* zErrMsg)
{
	int rc = 0;

	std::string * whocanbuy = new std::string("select * from accounts where balance > (select price from cars where id = " + std::to_string(carId) + ");");

	rc = sqlite3_exec(db, whocanbuy->c_str(), NULL, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		std::cout << "SQL error: " << zErrMsg << std::endl;
		sqlite3_free(zErrMsg);
		system("Pause");
	}
	
}

int main(void)
{
	int rc;
	sqlite3* db;
	char *zErrMsg = 0;
	bool flag = true;
	
	// connection to the database
	rc = sqlite3_open("carsDealer.db", &db);

	if (rc)
	{
		std::cout << "Can't open database: " << sqlite3_errmsg(db) << std::endl;
		sqlite3_close(db);
		system("Pause");
		return(1);
	}

	bool try1 = carPurchase(12,3, db, zErrMsg);
	bool try2 = carPurchase(12, 2, db, zErrMsg);
	bool try3 = carPurchase(12, 1, db, zErrMsg);

	//system("CLS");



	//bonus:
	std::string * bonus2 = new std::string("select color, count(*) as amountOfCars from cars group by color;");
	 
	rc = sqlite3_exec(db, bonus2->c_str(), callback, 0, &zErrMsg) != (SQLITE_OK);
	if (rc != SQLITE_OK)
	{
		std::cout << "SQL error: " << zErrMsg << std::endl;
		sqlite3_free(zErrMsg);
		system("Pause");
	}
	
	system("pause");
	//system("cls");

	sqlite3_close(db);
	
	return 0;
}